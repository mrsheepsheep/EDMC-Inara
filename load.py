# -*- coding: utf-8 -*-
import sys
import Tkinter as tk
import requests
import myNotebook as nb
import datetime
import keyring
from config import config
from os import listdir
from os.path import join

this = sys.modules[__name__]
this.s = None
this.prep = {}


def plugin_start():
    this.email = tk.StringVar(value=config.get("InaraEmail"))
    this.password = tk.StringVar(value=keyring.get_password("EDMC-Inara", config.get("InaraEmail")))
    this.online = False

    if login(this.email.get(), this.password.get()):
        this.online = True
    return 'Inara'


def plugin_app(parent):
    this.parent = parent
    label = tk.Label(parent, text="Inara:")
    this.status = tk.Label(parent, text="Disconnected", anchor=tk.W)
    if (this.online):
        updateInfo("Online")
    elif this.email.get():
        updateInfo("Cannot login !")
    return (label, this.status)


def plugin_prefs(parent):
    frame = nb.Frame(parent)
    frame.columnconfigure(1, weight=1)

    email_label = nb.Label(frame, text="Inara account email")
    email_label.grid(padx=10, row=10, sticky=tk.W)

    email_entry = nb.Entry(frame, textvariable=this.email)
    email_entry.grid(padx=10, row=10, column=1, sticky=tk.EW)

    pass_label = nb.Label(frame, text="Password")
    pass_label.grid(padx=10, row=12, sticky=tk.W)

    pass_entry = nb.Entry(frame, textvariable=this.password, show=u'•')
    pass_entry.grid(padx=10, row=12, column=1, sticky=tk.EW)

    if this.online:
        journal_upload_button = nb.Button(frame, text="Send journal", command=uploadJournal)
        journal_upload_button.grid(padx=10, row=14, sticky=tk.EW)
    return frame


# Log in
def login(email, password):
    data = {
        "loginid": email,
        "loginpass": password,
        "loginremember": "1",
        "formact": "ENT_LOGIN",
        "location": "intro"
    }
    url = "https://inara.cz/login"
    this.s = requests.Session()
    r = this.s.post(url, data=data)
    return len(r.cookies) == 2


# Settings dialog dismissed
def prefs_changed():
    if this.email.get() != config.get("InaraEmail") or this.password.get() != keyring.get_password("EDMC-Inara", this.email.get()) or not this.online:
        config.set("InaraEmail", this.email.get())
        keyring.set_password("EDMC-Inara", this.email.get(), this.password.get())
        this.online = login(this.email.get(), this.password.get())
        updateInfo(this.online and "Online" or "Cannot login !")


# Detect journal events
def journal_entry(cmdr, is_beta, system, station, entry, state):
	if not is_beta and this.online:
		if entry["event"] == "Progress": #rank progression
			setProgression(this.prep["Rank"], entry)
			
		elif entry["event"] == "Rank": #ranked up
			this.prep["Rank"] = entry
			print(this.prep)
			
		elif entry["event"] == "CapiShipBond": # killed a capship
			addLog(5, "Routed a Capital Ship from " + entry["VictimFaction"])
			
		elif entry["event"] == "PVPKill": # killed someone
			addLog(5, "Killed CMDR " + entry["Victim"].upper().encode("utf-8"))
			
		elif entry["event"] == "Died": # died to something
			killer = ""
			if "Killers" in entry.keys(): # died to CMDRs in a wing (possibly deprecated)
				killer = "CMDRs " + ", ".join(entry["Killers"]).upper()
			elif "KillerName" in entry: # died to one single thing not in a wing.
				killer = entry["KillerName"] if "KillerName_Localised" not in entry.keys() else entry["KillerName_Localised"]
			else: # died to a station, or a crash, or some other unnamed source.
				killer = "something..."
			addLog(5, "Died to " + killer.encode("utf-8").upper() )
			
		elif entry["event"] == "Bounty": 
			if entry["VictimFaction"] == "Thargoid": # added support for logging thargoid kills!
				addLog(5, "Killed a " + entry["VictimFaction"] + " " + entry["Target"])
				
# Update some data here too
def cmdr_data(data, is_beta):
	if not is_beta and this.online:
		cmdr_data.last = data
		setLocation(data["lastSystem"]["name"])
		setBalance(data["commander"]["credits"], 0)
		updateInfo("Data transmitted.")


# Defines location (system)
def setLocation(location):
	data = {
		"location": "cmdr",
		"formact": "USER_LOCATION_SET",
		"playercurloc": location
	}
	url = "https://inara.cz/cmdr"
	r = this.s.post(url, data=data)
	debug("Location updated")


# Defines balance & assets
def setBalance(credits, assets):
	data = {
		"location": "cmdr",
		"formact": "USER_CREDITS_SET",
		"playercredits": credits,
		"playercreditsassets": assets
	}
	url = "https://inara.cz/cmdr"
	r = this.s.post(url, data=data)
	debug("Balance updated.")


# Sets the current progression & ranks
def setProgression(r, p):
	data = {
		"location": "cmdr-ranks",
		"formact": "USER_USERRANKS_SET",
		"playerrep1[1]": r['Combat'],
		"playerrep1[2]": r['Trade'],
		"playerrep1[3]": r['Explore'],
		"playerrep1[4]": r['CQC'],
		"playerrep1[12]": r['Empire'],
		"playerrep1[13]": r['Federation'],
		"playerrep2[1]": p['Combat'],
		"playerrep2[2]": p['Trade'],
		"playerrep2[3]": p['Explore'],
		"playerrep2[4]": p['CQC'],
		"playerrep2[12]": p['Empire'],
		"playerrep2[13]": p['Federation'],
	}
	url = "https://inara.cz/cmdr-ranks"
	r = this.s.post(url, data=data)
	debug("Progression updated.")


# Adds an achievement log
# Types: 0 (Note), 1 (Exploration), 2 (Trading/Smuggling), 3 (Mission), 5 (Temporal), 6 (Financial)
def addLog(type, text):
	data = {
		"location": "cmdr-logs",
		"formact": "USER_ACHIEVEMENT_SET",
		"playerevent": "event5" + str(type),
		"playereventtext": text
	}
	url = "https://inara.cz/cmdr-logs"
	r = this.s.post(url, data=data)
	updateInfo("Log entry added")


def uploadJournal():
	# Get last journal file - Copied from monitory.py
	currentdir = config.get('journaldir') or config.default_journal_dir
	try:
		logfiles = sorted([x for x in listdir(currentdir) if x.startswith('Journal') and x.endswith('.log')],
			key=lambda x: x.split('.')[1:])
		logfile = logfiles and join(currentdir, logfiles[-1]) or None
		if logfile == None:
			return False
	except:
		logfile = None
		return False
	
	data = {
		"location": "cmdr",
		"formact": "JOURNAL_LOG_IMPORT"
	}
	url = "https://inara.cz/cmdr-cargo"
	r = this.s.post(url, data=data, files={"importfile[]": open(logfile, 'rb')})
	updateInfo("Journal uploaded")


def updateInfo(msg):
	this.status['text'] = datetime.datetime.now().strftime("%H:%M") + " - " + msg


def debug(msg):
	sys.stdout.write("[INARA] " + msg + "\n")


cmdr_data.last = None
