# Inara plugin for EDMC
A simple plugin that links your [Inara](https://inara.cz) account to [EDMC](https://github.com/Marginal/EDMarketConnector).

Once logged in, the plugin will:
* Update your credit balance (you should set your assets once manually)
* Update your Combat, Trade, Exploration, Empire, Federation & CQC Rank at startup. EDMC must be started before you start Open Play, Private group or Solo play in order to detect them.
* Add an achievement entry for various events, currently only destroying a Capital Ship and dying.
* Update your system location as you travel

This plugin can also upload your last Journal file to Inara, which updates:
* Rank & reputation
* Ships
* Credits
* Combat, flight and mission log
* Cargo & materials
* Your last location

Please do not abuse from this feature as it can flood Inara with big amounts of data.
The upload button is located in the Inara preferences tab.
Only upload your Journal once you finished playing !

# Installation
Download the [latest release](https://gitlab.com/mrsheepsheep/EDMC-Inara/repository/archive.zip?ref=master), open the archive (zip) and extract the folder (yup, the weird folder name, feel free to rename) to your EDMC plugin folder.

* Windows: `%LOCALAPPDATA%\EDMarketConnector\plugins` (usually `C:\Users\you\AppData\Local\EDMarketConnector\plugins`).
* Mac: `~/Library/Application Support/EDMarketConnector/plugins` (in Finder hold ⌥ and choose Go &rarr; Library to open your `~/Library` folder).
* Linux: `$XDG_DATA_HOME/EDMarketConnector/plugins`, or `~/.local/share/EDMarketConnector/plugins` if `$XDG_DATA_HOME` is unset.

You will need to re-start EDMC for it to notice the plugin. You can then switch to the Inara tab to login.

# Use with caution
Your Inara password is saved using the Keyring python library.

This plugin is not affiliated with Inara and therefore may break at any time.

# Personnal notes
[Companion data sample](http://www.jsoneditoronline.org/?id=c4be6a3d246713af75207a877c6c3f4e)

[Journal Manual](http://hosting.zaonce.net/community/journal/v11/Journal_Manual_v11.pdf)